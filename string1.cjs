function convertNumber(str){
    let neg = false;
    str = str.replace(',', '');
    if(str.indexOf('$') != -1  && str.indexOf('$')<=1){
        if(str.charAt(0) == '-'){
            neg = true;
        }

        str = str.substring(str.indexOf('$')+1, str.length);
    }else{
        return 0;
    }
    for(let idx = 0; idx<str.length; idx++){
        if(str.charAt(idx)<'0' || str.charAt(idx)>'9'){
            return 0;
        }
    }
    if(str.indexOf('.') == -1){
        str = parseInt(str);
    }else{
        str = parseFloat(str);
    }
    if(neg == true){
        str = str*(-1);
    }
    return str;

}

module.exports = convertNumber;