function dateMonth(dateString) {
    let date = new Date(dateString);
    // console.log(date);
    return date.getMonth()+1;
}

module.exports = dateMonth;