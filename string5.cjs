function arrayString(arr) {
    let resultString = '';
    for(let str in arr){
        resultString += arr[str]+' ';
    }
    return resultString.trim();   
}

module.exports = arrayString;
