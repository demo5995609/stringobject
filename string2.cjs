function convertIp(ip_address) {
    let arr = ip_address.split('.');
    let res = [];
    for(let idx = 0; idx<arr.length; idx++){
        if(isNaN(parseInt(arr[idx]))){
            return [];
        }
        res.push(parseInt(arr[idx]));
    }
    return res;
}
module.exports = convertIp;