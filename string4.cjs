// let obj = {"first_name": "JoHN", "last_name": "SMith"};
function fullNameTitleCase(obj) {
    
    let fullName = '';
    for(let key in obj){
        // console.log(key);
        let firstChar = obj[key].charAt(0).toUpperCase();
        let otherChar = obj[key].substring(1, key.length).toLowerCase();
        fullName = fullName+firstChar+otherChar+' ';
    
    }
    return fullName.trim();
}
module.exports = fullNameTitleCase;